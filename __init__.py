# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import res
from . import ir
import tryton_replication


def register():
    Pool.register(
        res.User,
        tryton_replication.ReplDeleted,
        tryton_replication.Model,
        tryton_replication.ReplicationLog,
        tryton_replication.Warning_,
        tryton_replication.UserLocalIdSrc,
        ir.Cron,
        module='replication_ss', type_='model')
