# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import requests
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.transaction import Transaction
from trytond.config import config
from trytond.rpc import RPC


class User(metaclass=PoolMeta):
    __name__ = 'res.user'

    upload_replica_db = fields.Boolean('Upload replica DB')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.__rpc__.update({
            'download_replica_db': RPC()
        })

    @staticmethod
    def default_upload_replica_db():
        return False

    @classmethod
    def download_replica_db(cls, repl_dbname, encrypted_pwd):
        user = cls(Transaction().user)
        dbname = Transaction().database.name

        # call route
        remote_host = config.get('replication', 'remote_host')
        url = 'http://%(host)s/%(dbname)s/replication/download_database' % {
            'host': remote_host,
            'dbname': repl_dbname,
        }
        params = {
            'host': remote_host,
            'user': user.login,
            'pwd': encrypted_pwd,
            'remote_database': dbname
        }
        r = requests.get(url=url, params=params)
        return r.content
